﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Test0513()
        {
            return View();
        }

        public ActionResult Test05132()
        {
            ViewData["school"] = "靜宜大學";
            return View();
        }

        public ActionResult Test05133()
        {
            ViewBag.phone = "0999-999-999";
            return View();
        }
    }
}