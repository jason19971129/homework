﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;


namespace WebApplication1.Models
{
    [MetadataType(typeof(UsersMetadata))]
    public partial class Users
    {
        
    }

    public class UsersMetadata
    {
        [Required(ErrorMessage = "必填欄位")]
        public int Id { get; set; }

        [DisplayName("姓名")]
        [Required(ErrorMessage = "必填欄位")]
        public string Name { get; set; }

        [DisplayName("電子郵件")]
        [Required(ErrorMessage = "必填欄位")]
        [EmailAddress]
        public string E_mail { get; set; }

        [DisplayName("密碼")]
        [Required(ErrorMessage = "必填欄位")]
        public string Password { get; set; }

        [DisplayName("出生日期")]
        [Required(ErrorMessage = "必填欄位")]
        public Nullable<System.DateTime> Birthday { get; set; }


        public Nullable<bool> Gender { get; set; }
    }
}